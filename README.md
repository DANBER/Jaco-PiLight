# Jaco-PiLight

Control of an LED-Strip (pixels can be switched individually). \
The LED-Strip is connected to an Arduino, which receives the colors via a serial USB connection.

<br>

What **you can learn** in this skill:

- Serial communication with an Arduino
- Manage processes in the background

**Complexity**: High

<br>

## Demo

<div align="center">
    <p>Color change</p>
    <img src="media/change_color.gif" alt="demo_1" height="300"/>
    <p></p>
    <p>Rainbow animation</p>
    <img src="media/rainbow_colors.gif" alt="demo_2" height="300"/>
</div>

<br>

## Installation

#### 1. Connect Arduino with LED-Strip

General procedure following: https://www.xgadget.de/anleitung/arduino-ws2812b-leds-mit-fastled-ansteuern/

The picture shows my used setup:

<div align="center">
    <img src="media/connections.jpg" alt="connections" height="500"/>
</div>

#### 2. Install Skill

Add the skill to your assistant.

#### 3. Installation on Arduino

Install the program `Arduino/PiLight.ino` on the Arduino. \
You may need to adjust some parameters in the file.

##### Installation with preinstalled Arduino-Cli:

Connect to container:

```bash
docker run --network host --rm \
  --volume `pwd`/skills/skills/Jaco-PiLight/:/Jaco-Master/skills/skills/Jaco-PiLight/ \
  --device /dev/ttyUSB0 \
  -it skill_jaco_pilight_arm64
```

Compile and upload the program to the Arduino:

```bash
arduino-cli compile --fqbn arduino:avr:uno Jaco-PiLight/Arduino/ -v
arduino-cli upload -p /dev/ttyUSB0 --fqbn arduino:avr:uno Jaco-PiLight/Arduino/ -v
```

If you have a board other than Arduino-Uno you might need to change the board library in the container: \
(Run in container)

```bash
# Print boards and library names
arduino-cli board listall
arduino-cli core search arduino

# Uninstall the preinstalled version first, you might get compilation errors else
arduino-cli core uninstall arduino:avr

arduino-cli core install [YourBoard]
arduino-cli core list
```

Test it:

```bash
python3 /Jaco-Master/skills/skills/Jaco-PiLight/Arduino/test-lights.py
```

## Debugging

Build container image:

```bash
sudo docker run --security-opt label=disable --rm --privileged multiarch/qemu-user-static --reset -p yes
docker buildx build --platform=linux/arm64 -t skill_jaco_pilight_arm64 - < skills/skills/Jaco-PiLight/Containerfile_arm64
```

Run skill: \
(Assumes mqtt-broker already running)

```bash
docker run --network host --rm \
  --volume `pwd`/skills/skills/Jaco-PiLight/:/Jaco-Master/skills/skills/Jaco-PiLight/ \
  --volume `pwd`/skills/skills/Jaco-PiLight/skilldata/:/Jaco-Master/skills/skills/Jaco-PiLight/skilldata/ \
  --volume `pwd`/userdata/config/:/Jaco-Master/skills/skills/userdata/config/:ro \
  --device /dev/ttyUSB0 \
  -it skill_jaco_pilight_arm64 python3 /Jaco-Master/skills/skills/Jaco-PiLight/action-light.py
```
