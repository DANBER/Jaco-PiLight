#include <Adafruit_NeoPixel.h>

#define PIN 6
#define NUMPIXELS 60

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

void setup()
{
  pixels.begin();
  Serial.begin(9600);
}

void loop()
{
  if (Serial.available() >= 4)
  {
    int color[4];
    for (int i = 0; i < 4; i++)
    {
      byte nr = Serial.read();
      color[i] = nr;
    }
    set_color(color[0], color[1], color[2], color[3]);
  }
}

void set_color(int pixel_id, int red, int green, int blue)
{
  pixels.setPixelColor(pixel_id, pixels.Color(red, green, blue));
  pixels.show();
}
