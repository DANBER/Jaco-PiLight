import itertools
import time

import serial

# ==================================================================================================

ser = serial.Serial("/dev/ttyUSB0", 9600)  # Adjust name if necessary
# The Arduino resets after a serial connection, so you have to wait a short time
time.sleep(5)

values = [[0, 225, 0, 0], [1, 225, 0, 0], [2, 225, 0, 0]]
values = bytearray(list(itertools.chain.from_iterable(values)))
ser.write(values)
print("Set first three leds to red")
time.sleep(3)
values = [[0, 0, 0, 0], [1, 0, 0, 0], [2, 0, 0, 0]]
values = bytearray(list(itertools.chain.from_iterable(values)))
ser.write(values)
print("Set first three leds to black")
