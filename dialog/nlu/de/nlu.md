## intent:set_delay
- Setze den Delay auf [25](skill_dialogs-numbers2hundred)
- Stelle Verzögerung auf [1000](skill_dialogs-numbers2hundred) ein
- Stelle Verzögerung auf [5](skill_dialogs-numbers2hundred)
- Setze die Verzögerung auf [100](skill_dialogs-numbers2hundred)

## intent:set_brightness
- Setze die Helligkeit auf [75](skill_dialogs-numbers2hundred)
- Setze die Lampe auf [20](skill_dialogs-numbers2hundred)
- Dimme das Licht auf [80](skill_dialogs-numbers2hundred) (Prozent|)
- Stelle die Licht Intensität auf [40](skill_dialogs-numbers2hundred) (Prozent|)

## intent:light_on
- (Lampe|Licht) an
- Mach (das Licht|die Lampe) an
- Ich mag Licht

## intent:light_off
- Mach (das Licht|die Lampe) aus
- (Lampe|Licht) aus
- Ich mag es dunkel

## lookup:color
color.txt

## intent:set_color
- Setze die Farbe auf [Schwarz](color.txt)
- Farbe auf [Regenbogen](color.txt)
- Mache die Farbe [Türkis](color.txt)
- Schalte die Lampe auf [Grün](color.txt)
- Mache das Licht [Blau](color.txt)
- Setze die Lampe auf [Rot](color.txt)
