## intent:set_delay
- Set the delay to [25](skill_dialogs-numbers2hundred)
- Set delay to [1000](skill_dialogs-numbers2hundred)
- Set the delay to [100](skill_dialogs-numbers2hundred)

## intent:set_brightness
- Set the brightness to [75](skill_dialogs-numbers2hundred)
- Set the lamp to [20](skill_dialogs-numbers2hundred)
- Dim the light to [80](skill_dialogs-numbers2hundred) (percent|)
- Set light intensitiy to [40](skill_dialogs-numbers2hundred) (percent|)

## intent:light_on
- (Lamp|Light) on
- Turn on the (lamp|light)
- I like light

## intent:light_off
- Turn off the (lamp|light)
- (Lamp|Light) off
- I like it dark

## lookup:color
color.txt

## intent:set_color
- Set the color to [Black](color.txt)
- Color to [Rainbow](color.txt)
- Make the color [Turquoise](color.txt)
- Switch the lamp to [Green](color.txt)
- Make the light [Blue](color.txt)
- Set the lamp to [Red](color.txt)
