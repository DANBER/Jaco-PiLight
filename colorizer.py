import colorsys
import copy
import threading
import time

import serial

# ==================================================================================================

ser: serial.Serial
max_brightness: int
number_leds: int
dimming_brightness = 100
write_delay = 10
current_colors = None
running = False
mutex = threading.Lock()


# ==================================================================================================


def init(path, num_leds, max_bright):
    """Open a serial connection to the Arduino"""
    global ser, number_leds, max_brightness, current_colors

    ser = serial.Serial(path, 9600)
    number_leds = num_leds
    max_brightness = max_bright
    current_colors = make_simple_color(0, 0, 0)

    if not ser.isOpen():
        ser.open()

    # Make all pixels black at first
    run_function_threaded(run_function_delayed, (update_colors, 5))


# ==================================================================================================


def run_function_threaded(function_name, args=()):
    """Runs a function in a new thread"""

    thread = threading.Thread(target=function_name, args=args)
    thread.daemon = True
    thread.start()


# ==================================================================================================


def run_function_delayed(function_name, delay):
    """Runs a function after a delay"""

    time.sleep(delay)
    function_name()


# ==================================================================================================


def make_simple_color(red, green, blue):
    """Set all pixels to same color"""

    pixel_colors = []
    for i in range(number_leds):
        pixel_colors.append([i, red, green, blue])

    return pixel_colors


# ==================================================================================================


def set_dimming(percent):
    """Update brightness used for dimming light"""
    global dimming_brightness

    if 0 <= percent <= 100:
        dimming_brightness = percent
        if not running:
            # Update colors only if there is no thread running which updates colors
            run_function_threaded(update_colors)
        return True
    else:
        return False


# ==================================================================================================


def set_delay(miliseconds):
    """Update delay between pixel changes"""
    global write_delay

    if 0 <= miliseconds <= 1000:
        write_delay = miliseconds
        return True
    else:
        return False


# ==================================================================================================


def update_brightness(colors):
    """Makes colors less bright depending on max_brightness"""

    for c in colors:
        for i in range(1, 4):
            c[i] = int(c[i] * max_brightness / 255 * dimming_brightness / 100)

    return colors


# ==================================================================================================


def set_color(color):
    """Set a simple color"""
    global current_colors, running

    color = color.lower()
    running = False

    if color == "red":
        current_colors = make_simple_color(255, 0, 0)
        run_function_threaded(update_colors)
    elif color == "green":
        current_colors = make_simple_color(0, 255, 0)
        run_function_threaded(update_colors)
    elif color == "blue":
        current_colors = make_simple_color(0, 0, 255)
        run_function_threaded(update_colors)
    elif color == "white":
        current_colors = make_simple_color(255, 255, 255)
        run_function_threaded(update_colors)
    elif color == "black":
        current_colors = make_simple_color(0, 0, 0)
        run_function_threaded(update_colors)
    elif color == "orange":
        current_colors = make_simple_color(255, 153, 0)
        run_function_threaded(update_colors)
    elif color == "turquoise":
        current_colors = make_simple_color(0, 204, 153)
        run_function_threaded(update_colors)
    elif color == "pink":
        current_colors = make_simple_color(255, 0, 255)
        run_function_threaded(update_colors)
    elif color == "rainbow":
        running = True
        run_function_threaded(run_rainbow)
    else:
        return False

    return True


# ==================================================================================================


def run_rainbow():
    """Update all pixels of the led stripe with rainbow colors"""
    global current_colors

    k = 0
    while True:
        new_colors = []
        for i in range(number_leds):
            red, green, blue = hue_to_rgb(k + i * 2)
            new_colors.append([i, red, green, blue])

        k = (k + 5) % 360
        if running:
            current_colors = new_colors
            update_colors()
        else:
            break


# ==================================================================================================


def update_colors():
    """Update all pixels of the led stripe"""

    colors = copy.deepcopy(current_colors)
    colors = update_brightness(colors)

    for c in colors:
        write_to_serial(c[0], c[1], c[2], c[3])
        time.sleep(write_delay / 1000)


# ==================================================================================================


def write_to_serial(pixel_id, color_red, color_green, color_blue):
    """Send a pixel color to the arduino"""

    mutex.acquire()
    try:
        values = bytearray([pixel_id, color_red, color_green, color_blue])
        ser.write(values)
    finally:
        mutex.release()


# ==================================================================================================


def hue_to_rgb(hue_val):
    """Calculate rgb color from hue value"""

    hue_val = hue_val % 360
    red, green, blue = colorsys.hsv_to_rgb(hue_val / 360, 1, 1)
    red = int(red * 255)
    green = int(green * 255)
    blue = int(blue * 255)
    return red, green, blue
