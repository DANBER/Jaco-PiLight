import os

import colorizer

from jacolib import assistant

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
assist: assistant.Assistant


# ==================================================================================================


def callback_light_on(message):
    """Callback to turn light on"""
    colorizer.set_color("white")


# ==================================================================================================


def callback_light_off(message):
    """Callback to turn light off"""

    colorizer.set_color("black")


# ==================================================================================================


def callback_set_color(message):
    """Callback to set a simple color"""

    slots = assist.extract_entities(message, "jaco_pilight_color")
    if len(slots) == 1:
        color = slots[0]

        result = colorizer.set_color(color)
        if result:
            result_sentence = ""
        else:
            result_sentence = assist.get_random_talk("color_not_allowed")
    else:
        result_sentence = assist.get_random_talk("color_not_understood")

    if result_sentence != "":
        assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_set_brightness(message):
    """Callback to set a brightness value to the light"""

    slots = assist.extract_entities(message, "skill_dialogs-numbers2hundred")
    if len(slots) == 1:
        value = int(slots[0])

        result = colorizer.set_dimming(value)
        if result:
            result_sentence = ""
        else:
            result_sentence = assist.get_random_talk("brightness_not_allowed")
    else:
        result_sentence = assist.get_random_talk("brightness_not_understood")

    if result_sentence != "":
        assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_set_delay(message):
    """Callback to set a delay between pixel changes"""

    slots = assist.extract_entities(message, "skill_dialogs-numbers2hundred")
    if len(slots) == 1:
        value = int(slots[0])

        result = colorizer.set_delay(value)
        if result:
            result_sentence = ""
        else:
            result_sentence = assist.get_random_talk("delay_not_allowed")
    else:
        result_sentence = assist.get_random_talk("delay_not_understood")

    if result_sentence != "":
        assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def main():
    global assist

    assist = assistant.Assistant(repo_path=file_path)

    port = assist.get_config()["user"]["arduino_port"]
    num_leds = int(assist.get_config()["user"]["number_leds"])
    max_bright = int(assist.get_config()["user"]["max_brightness"])
    colorizer.init(port, num_leds, max_bright)

    assist.add_topic_callback("light_on", callback_light_on)
    assist.add_topic_callback("light_off", callback_light_off)
    assist.add_topic_callback("set_color", callback_set_color)
    assist.add_topic_callback("set_brightness", callback_set_brightness)
    assist.add_topic_callback("set_delay", callback_set_delay)
    assist.run()


# ==================================================================================================

if __name__ == "__main__":
    main()
